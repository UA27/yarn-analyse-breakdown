# yarn-analyse-breakdown

![](images/main_view.png)

### Core concepts
---
1. #### Nodes
    * title
    * body summary
    * tags
    
    ![](images/main_view_node_anatomy.png)
---
2. #### Node Controls
    * delete node
    * change node header colour

    ![](images/main_view_node_controls.png)
---
3. #### Edges
    * link arrows (edges)
    
    ![](images/main_view_edges.png)
---
4. #### Menu
    * file (project data)
    * new node
    
    ![](images/main_view_menu.png)
---
5. #### Search and Alignment
    * search titles
    * search bodies
    * search tags
    * align selection horizontally
    * align selection vertically
    
    ![](images/main_view_search_and_controls.png)
---
6. #### View Controls
    * `arrows` and `wasd` to move camera around
    * `mousewheel` to zoom
    * drag `leftmouse` on canvas to group select
    * drag `leftmouse` on node to move around
    * down `rightmouse` to create a new node
    * `space` to reset camera at (0, 0)

    <br>
    <br>
    <br>

# Yarn Node Editor
![](images/node_editor.png)

### Core concepts
---
1. #### State Controls
    * visibility state (enabled/disabled) (hidden/visible)
    * double down `mouseleft` on node to open
    * down `mouseany` outside of model to close
---
2. #### Form
    * title input field (inline text input)
    * tags input field (inline text input)
    * text body field (highlighted textarea)
    * current (selected) line coloured

    ![](images/node_editor_form.png)
---
3. #### Model Window
    * text body field length (char count)
    * text body field lines (line count)
    * text body field position (line index and column index)

    ![](images/node_editor_model.png)

<br>
<br>
<br>

# Yarn Data File

### Core concepts
---
1. #### Json Parse
    * json generic object array
    * five properties
        + title: string
        + tags: string
        + body: string
        + position: object (x: int, y: int)
        + colorID: int

    ![](images/data_file_json.png)
---
2. #### Yarn.txt Parse
    * monolithic text structure
    * 5 fields, 2 separators
        + node start: "==="
        + title: format (title: "")
        + tags: format (tags: "")
        + colorID: format (colorID: "{0}", int)
        + position: format (position: "{0},{1}", int, int)
        + body start: ---
        + body: format ("")
        + node end: "==="

    ![](images/data_file_yarn.txt.png)
---
